import Vue from "vue";
import VueRouter from "vue-router";
import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
import Dashboard from "./components/Dashboard.vue";
import Reports from "./components/Reports.vue";

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: '/fleet',component: require('./components/sgr/fleet/Fleet.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/fleet/create',component: require('./components/sgr/fleet/Create.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/towns',component: require('./components/sgr/towns/Towns.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/towns/create',component: require('./components/sgr/towns/Create.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/employees',component: require('./components/sgr/employees/Employees.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/employees/create',component: require('./components/sgr/employees/Create.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/departments',component: require('./components/sgr/departments/Department.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/departments/create',component: require('./components/sgr/departments/Create.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/schedules',component: require('./components/sgr/Schedules.vue'),
            meta: {
                forAuth: true
            }
        },
        { path: '/bookings',component: require('./components/sgr/Bookings.vue'),
            meta: {
                forAuth: true
            }
        },





        { path: '/login',component: Login,
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/register',
            component: Register,
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/book',
            component: require('./components/Book.vue'),
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/dashboard',
            component: Dashboard,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/reports',
            component: Reports,
            meta: {
                forAuth: true
            }
        },{
            path: '/create',
            component: require('./components/Create.vue'),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/users/:user/edit',
            component: require('./components/Edit.vue'),
            meta: {
                forAuth: true
            }
        },
        {
            path:'admins',
            component:require('./components/Users.vue'),
            meta:{
                forAuth: true
            }
        },
        {
            path: '/logout',
            component: require('./components/Logout.vue'),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/how-it-works',
            component: require('./components/How-it-works.vue'),
            meta: {
                forAuth: true
            }
        },{
            path: '/profile',
            component: require('./components/Profile.vue'),
            meta: {
                forAuth: true
            },
        },{
            path: '/emits',
            component: require('./components/Massage.vue'),
            meta: {
                forAuth: true
            },
        }
    ],
    linkActiveClass:'active',
    mode:'history'
});

export default router;
