import Vue from 'vue';
import App from './App.vue'
import Router from './router';
import Auth from './packages/auth/Auth'
import ViewResource from 'vue-resource';
import swal from 'sweetalert';
import VeeValidate from 'vee-validate';

Vue.use(Auth);
Vue.use(VeeValidate);
Vue.use(ViewResource);

Vue.http.options.root = 'http://localhost/muhoho/public/api';
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();
Vue.http.interceptors.push((request,next) =>{
    next(response=>{
        if(response.status == 401){
        swal("Error!", response.body.error , "error")
        }
    });
});



Router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.isAuthenticated()) {
                next({
                    path: '/dashboard'
                });
            } else next();
        } else if (to.matched.some(record => record.meta.forAuth)) {
            if (!Vue.auth.isAuthenticated()) {
                next({
                    path: '/login'
                });
            } else next();
        }
        else next();
    }
);
new Vue({
    el: '#app',
    render: h => h(App),
    router: Router
});
